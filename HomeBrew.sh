#!/bin/bash

# HomeBrew is apt-get for macOS
# Website: https://brew.sh/

clear
echo "Install Scripts by @KartSriv: HomeBrew"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 3 [--------------------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: HomeBrew"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 2 [##########----------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: HomeBrew"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 1 [####################------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: HomeBrew"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 0 [###########################]"
sleep 1
echo "<-------------Install Started-------------->"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
echo "<-------------Install Finished------------->"
echo "Have a good time, hacking!"
