#!/bin/bash

# GTK is for GUI
# GitHub: http://gtk.org
# Source: http://macappstore.org/gtk/

clear
echo "Install Scripts by @KartSriv: GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 3 [--------------------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 2 [##########----------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 1 [####################------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 0 [###########################]"
sleep 1
echo "<-------------Install Started-------------->"
# ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null -----------> HomeBrew
brew install gtk+
echo "<-------------Install Finished------------->"
echo "Have a good time, hacking!"
