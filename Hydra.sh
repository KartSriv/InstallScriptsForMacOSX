#!/bin/bash

# Hydra GTK is a Brute Force Tool(GUI)
# GitHub: https://github.com/vanhauser-thc/thc-hydra/tree/master/hydra-gtk
# Source: http://macappstore.org/hydra/

clear
echo "Install Scripts by @KartSriv: Hydra-GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 3 [--------------------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: Hydra-GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 2 [##########----------------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: Hydra-GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 1 [####################------]"
sleep 1
clear
echo "Install Scripts by @KartSriv: Hydra-GTK"
echo "Supported OS(s): Linux and macOS with git"
echo "Install in 0 [###########################]"
sleep 1
echo "<-------------Install Started-------------->"
# ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null -----------> HomeBrew
brew install hydra
echo "<-------------Install Finished------------->"
echo "Have a good time, hacking!"
